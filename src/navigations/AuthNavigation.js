import { createStackNavigator } from 'react-navigation-stack'
import Login from '../screens/FirstScreens/screens/Login'
import Signup from '../screens/FirstScreens/screens/Signup'
import ForgetPassword from '../screens/FirstScreens/screens/ForgetPassword'
import NewPassword from '../screens/FirstScreens/screens/NewPassword'


const AuthNavigation = createStackNavigator(
  {
    Login: { screen: Login },
    Signup: { screen: Signup },
    ForgetPassword: { screen: ForgetPassword },
    NewPassword: { screen: NewPassword }
    
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none'
  }
)

export default AuthNavigation



