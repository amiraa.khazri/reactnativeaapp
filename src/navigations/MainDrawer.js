import "react-native-gesture-handler";

import * as React from "react";

import { NavigationContainer, DefaultTheme  } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Icon from "react-native-vector-icons/Ionicons";
import Navigation from "../screens/RealTimeScreen/Navigation";
import { DrawerContent } from "../screens/DrawerContent";

import MissionsScreenIndex from "../screens/MissionsScreen/MissionsScreenIndex";
import AlarmsScreenIndex from "../screens/AlarmsScreen/AlarmsScreenIndex";
import CalendarScreenIndex from "../screens/CalendarScreen/CalendarScreenIndex";
import ContactScreenIndex from "../screens/ContactScreen/ContactScreenIndex";
import ExpensesScreenIndex from "../screens/ExpensesScreen/ExpensesScreenIndex";
import LocationScreenIndex from "../screens/LocationsScreen/LocationScreenIndex";
import TripsandStopsScreenIndex from "../screens/TripsandStopsScreen/TripsandStopsScreenIndex";
import Login from '../screens/FirstScreens/screens/Login';

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'rgb(255, 45, 85)',
  },
};

const MissionsStack = createStackNavigator();
const MissionsStackScreen = ({ navigation }) => (
  <MissionsStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#71415E",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <MissionsStack.Screen
      name="Missions"
      component={MissionsScreenIndex}
      options={{
        title: "Missions",
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#71415E"
            onPress={() => navigation.openDrawer()}
          >
            {" "}
          </Icon.Button>
        ),
      }}
    />
  </MissionsStack.Navigator>
);

const CalendarStack = createStackNavigator();
const CalendarStackScreen = ({ navigation }) => (
  <CalendarStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#71415E",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <CalendarStack.Screen
      name="Calendar"
      component={CalendarScreenIndex}
      options={{
        title: "Calendar",
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#71415E"
            onPress={() => navigation.openDrawer()}
          >
            {" "}
          </Icon.Button>
        ),
      }}
    />
  </CalendarStack.Navigator>
);

const TripsandStopsStack = createStackNavigator();
const TripsandStopsStackScreen = ({ navigation }) => (
  <TripsandStopsStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#71415E",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <TripsandStopsStack.Screen
      name="TripsandStops"
      component={TripsandStopsScreenIndex}
      options={{
        title: "Trips and Stops",
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#71415E"
            onPress={() => navigation.openDrawer()}
          >
            {" "}
          </Icon.Button>
        ),
      }}
    />
  </TripsandStopsStack.Navigator>
);

const LocationsStack = createStackNavigator();
const LocationsStackScreen = ({ navigation }) => (
  <LocationsStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#71415E",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <LocationsStack.Screen
      name="Locations"
      component={LocationScreenIndex}
      options={{
        title: "Locations",
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#71415E"
            onPress={() => navigation.openDrawer()}
          >
            {" "}
          </Icon.Button>
        ),
      }}
    />
  </LocationsStack.Navigator>
);

const ExpensesStack = createStackNavigator();
const ExpensesStackScreen = ({ navigation }) => (
  <ExpensesStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#71415E",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <ExpensesStack.Screen
      name="Expenses"
      component={ExpensesScreenIndex}
      options={{
        title: "Expenses",
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#71415E"
            onPress={() => navigation.openDrawer()}
          >
            {" "}
          </Icon.Button>
        ),
      }}
    />
  </ExpensesStack.Navigator>
);

const AlarmsStack = createStackNavigator();
const AlarmsStackScreen = ({ navigation }) => (
  <AlarmsStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#71415E",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <AlarmsStack.Screen
      name="Alarms"
      component={AlarmsScreenIndex}
      options={{
        title: "Alarms",
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#71415E"
            onPress={() => navigation.openDrawer()}
          >
            {" "}
          </Icon.Button>
        ),
      }}
    />
  </AlarmsStack.Navigator>
);

const ContactStack = createStackNavigator();
const ContactStackScreen = ({ navigation }) => (
  <ContactStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#71415E",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <ContactStack.Screen
      name="Contact"
      component={ContactScreenIndex}
      options={{
        title: "Alarms",
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#71415E"
            onPress={() => navigation.openDrawer()}
          >
            {" "}
          </Icon.Button>
        ),
      }}
    />
  </ContactStack.Navigator>
);



const Drawer = createDrawerNavigator();

const Index = () => {
  return (
    <NavigationContainer >
      <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
        <Drawer.Screen name="RealTimeTracking" component={Navigation} />
        <Drawer.Screen name="MissionsScreen" component={MissionsStackScreen} />
        <Drawer.Screen name="CalendarScreen" component={CalendarStackScreen} />
        <Drawer.Screen name="TripsandStopsScreen" component={TripsandStopsStackScreen}/>
        <Drawer.Screen name="LocationScreen" component={LocationsStackScreen} />
        <Drawer.Screen name="ExpensesScreen" component={ExpensesStackScreen} />
        <Drawer.Screen name="AlarmsScreen" component={AlarmsStackScreen} />
        <Drawer.Screen name="ContactScreen" component={ContactStackScreen} />
        <Drawer.Screen name="Login" component={Login} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default Index;
