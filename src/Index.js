import React from "react";
import { Provider } from "react-redux";
import MainDrawer from "./navigations/MainDrawer";

import { store } from "./screens/AlarmsScreen/store/Store";
export default function App() {
  return (
    <Provider store={store}>
      <MainDrawer />
    </Provider>
  );
}
