import React ,{ useState }from "react";
import {Modal, View, StyleSheet, Image, TouchableOpacity, ScrollView, ToastAndroid } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Icons from "react-native-vector-icons/Ionicons";
import { Button, TextInput } from 'react-native-paper';

import {
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  Text,
  TouchableRipple,
  Switch,
} from "react-native-paper";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";

export function DrawerContent(props) {

  const [modalOpen, setModalOpen] = useState(false);
 
  return (
    
    <View style={{ flex: 1 }}>
      

      <DrawerContentScrollView {...props}>

      {/* sidebar */}
    <View   style={{alignItems: "center", flex: 1}}>
      <Image source={require("../../assets/pic.jpg")} style={styles.profileImg}/>
      <Text style={{fontWeight:"bold",fontSize:16,marginTop:10, alignItems: "center"}}>Amira Khazri</Text>
      <Text h3 gray medium  style={{color:"gray",marginBottom:10, alignItems: "center"}}>Amira@kh.com</Text>
     
      {/* Modal */}
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center",}}>
        
          <Modal transparent visible={modalOpen} animationType="slide">
              <View style={styles.ModelContent}>
                
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => setModalOpen(false)}>
                    <Icon name="close-outline" color={'#800080'} size={20} /> 
                </TouchableOpacity>
              
                <Text style={{fontWeight:"bold",fontSize:25,marginTop:11, color:"#5f465c"}}>Update profile</Text>
                <Text style={{color:"gray",marginBottom:5,fontSize:18, alignItems: "center"}}>Amira@kh.com</Text>
                <View style={{ width: 350,marginTop:13, borderWidth:0.2, }}></View>
                <ScrollView >
                <TextInput mode="outlined" placeholder="First name * " style={styles.inputText} />
                <TextInput mode="outlined" placeholder="Last name *"  style={styles.inputText} />
                <TextInput mode="outlined" placeholder="Mobile number *"  style={styles.inputText}/>
               
                <TextInput autoCompleteType  ="password"  mode="outlined"   placeholder="Old Pasword"  style={styles.inputText}/>
                <TextInput mode="outlined" placeholder="New Pasword"  style={styles.inputText}/>
                <TextInput mode="outlined" placeholder="Confirm password"  style={styles.inputText}/>
               </ScrollView>
                <Button mode="outlined" style={{ width: 100,margin:15, height:45,borderWidth:2, borderColor:'#5f465c',}}  onPress={() => ToastAndroid.showWithGravityAndOffset('Updated...',ToastAndroid.LONG,ToastAndroid.CENTER, 25, 50)}>
                 <Text style={{fontWeight:"bold",fontSize:16,marginTop:11, color:"#5f465c"}}> Save </Text>
                 </Button>
                
              </View>
            </Modal>
        
      <TouchableOpacity  style={styles.listItem} onPress={()=> setModalOpen(true)}>
        <Icons  name= "md-settings"  color={'#800080'} size={17} style={{ marginBottom :9 }}/>
      </TouchableOpacity>
      </View>


    </View>
      <View style={styles.sidebarDivider}></View>


        <View style={styles.drawercontent}>
          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem 

              icon={({ color, size }) => (
                <Icon name="map-clock-outline" color={'#800080'} size={size} />
              )}
              label="Real Time Tracking"
              onPress={() => {
                props.navigation.navigate("RealTimeTracking");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="card-text-outline" color={'#800080'} size={size} />
              )}
              label="Mission"
              onPress={() => {
                props.navigation.navigate("MissionsScreen");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="calendar-outline" color={'#800080'} size={size} />
              )}
              label="Calendar"
              onPress={() => {
                props.navigation.navigate("CalendarScreen");
              }}
            />

            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="map-marker-path" color={'#800080'} size={size} />
              )}
              label="Trips and Stops"
              onPress={() => {
                props.navigation.navigate("TripsandStopsScreen");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="map-marker-radius" color={'#800080'} size={size} />
              )}
              label="Locations"
              onPress={() => {
                props.navigation.navigate("LocationScreen");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="currency-usd" color={'#800080'} size={size} />
              )}
              label="Expenses"
              onPress={() => {
                props.navigation.navigate("ExpensesScreen");
              }}
            />

            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="bell-alert-outline" color={'#800080'} size={size} />
              )}
              label="Alarms (n)"
              onPress={() => {
                props.navigation.navigate("AlarmsScreen");
              }}
            />
          </Drawer.Section>

          <Drawer.Section>
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="account-check-outline" color={'#800080'} size={size} />
              )}
              label="Contact"
              onPress={() => {
                props.navigation.navigate("ContactScreen");
              }}
            />
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>

      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({ color, size }) => (
            <Icon name="exit-to-app" color={'#800080'} size={size} />
          )}
          label="Sign Out"
          onPress={() => {  props.navigation.navigate("Login"); }}
        />
      </Drawer.Section>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: "#f4f4f4",
    borderTopWidth: 1,
   
  },
  TopDrawerSection: {
    marginBottom: 12,
    marginTop: 9,
    borderBottomColor: "#f4f4f4",
    borderBottomWidth: 1,
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  logo: {
    width: 200,
    height: 100,
    flex: 1,
    //borderRadius:40,
    //borderWidth:40,
  },  
  sidebarDivider:{
    height:1,
    width:"100%",
    backgroundColor:"#800080",
    marginVertical:-3,
  
  },
  profileImg:{
    width:60,
    height:60,
    borderRadius:40,
    
 
  },
  ModelContent: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#b79cab",
    borderRadius:90,
    margin:5,
    justifyContent: "center"
  },
  inputText :{
    width: 350,
    marginTop:18, 
    height:50,
    backgroundColor: "#b79cab",
    
   
   
    
  },

});
