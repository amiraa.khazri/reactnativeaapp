import React from "react";
import { View, Text } from "react-native";

const ContactScreenIndex = () => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Contact</Text>
    </View>
  );
};

export default ContactScreenIndex;
