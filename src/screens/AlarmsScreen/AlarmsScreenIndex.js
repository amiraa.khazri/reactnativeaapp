import React from "react";
import { View, Text, Button,TextInput } from "react-native";
import { SearchBar } from "react-native-elements";
// import { TextInput } from "react-native-paper";
import {Ionicons} from '@expo/vector-icons'
import { FlatList, ScrollView } from "react-native-gesture-handler";
// import { useSelector, useDispatch } from "react-redux";
// import { check_redux } from "./store/reducer";



export default class AlarmsScreenIndex extends React.Component{

  state = {
    alarms: []
  }
// example for testing
  async componentDidMount(){

      let url = 'https://api.mocki.io/v1/b043df5a'
      fetch(url)
      .then((response) => response.json())
      .then((json) => {
          console.log('JSON', json)
          this.setState({alarms: json, filterAlarms: json})
      } )
      .catch((error) => {
        console.error(error);
        this.setState({alarms: []})
      });
  }

  onChangeText(text){
    console.log('textChanged', text)
    let filterArray = this.state.filterAlarms
    let searchResult = filterArray.filter(alarms => 
      alarms.name.includes(text)
      )
      this.setState({ alarms: searchResult })
  }

  render(){

    return(

    // SearchBar

      <View style={{
        flex:1, 
        backgroundColor:'#f1e5ee',
        
         }}>
      <View>
        <TextInput
          placeholder="Search..."
          style={{
            
              margin:15,
              padding:6,
              paddingLeft:33,
              borderWidth:0,
              borderColor:'#90486e',
              borderRadius:50,
              fontSize:16,
              backgroundColor:'#fff'
             
          }}
        onChangeText={text => this.onChangeText(text)}

        />
          <Ionicons
           style={{ 
             position:'absolute',
             left:20,
             top: 26
             }}
           name="ios-search"   
             size={20}
             color="#90486e"

           />

      </View>

      {/* List Alarms */}

      <FlatList 
        data={this.state.alarms}
        renderItem={({ item }) => (
          <View style={{ 
            alignItems:'center',
            justifyContent:'center',
            marginHorizontal:10,
            marginVertical:5,
            borderRadius:8,
            borderColor:'#90486e',
            borderWidth: 2,
            padding:10,
            backgroundColor:'#71415E',
            
           
           }}>
            <Text style={{
              fontSize:15,
              fontWeight: 'bold',
              marginBottom: 5,
              color:'#fff'
            
            }}>{item.name}</Text>
            <Text style={{color:'#fff',}}>{item.city}</Text>
            {/* <Button title='ok'></Button> */}
          </View>
        )}

        ListEmptyComponent={() =>
        
          <View style={{ alignItems:'center',
           justifyContent:'center',}}>
            <Text>Not Found ...</Text>
          </View>
        
        }

      />
     
      
    </View>
    )

  }

}





