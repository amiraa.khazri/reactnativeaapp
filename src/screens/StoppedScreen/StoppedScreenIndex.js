import React from "react";
import { View, Text } from "react-native";

const StoppedScreenIndex = () => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Stopped Trucks</Text>
    </View>
  );
};

export default StoppedScreenIndex;
