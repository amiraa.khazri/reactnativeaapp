import * as React from "react";
import { Button, TextInput, View } from "react-native";
import { Formik } from "formik";

export default function ModalAdd() {
  return (
    <View>
      <Formik initialValues={{ vehicule: "" }}>
        onSubmit=
        {(values) => {
          console.log(values);
        }}
        >
        {(formikprops) => (
          <View>
            <TextInput
              placeholder="vehicule"
              onChangeText={formikprops.handleChange("vehicule")}
              value={formikprops.values.vehicule}
            />
            <Button title="submit" onPress={formikprops.handleSubmit} />
          </View>
        )}
      </Formik>
    </View>
  );
}
