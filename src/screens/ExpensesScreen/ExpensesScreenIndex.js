import React, { useState } from "react";
import { render } from "react-dom";
import VegaScrollList from 'react-native-vega-scroll-list';
import { Modal, View, Text, TouchableOpacity, FlatList, StyleSheet, ScrollView } from "react-native";
import { Button, shadow, TextInput } from "react-native-paper";
import Icon  from 'react-native-vector-icons/Ionicons'
import Icons from "react-native-vector-icons/Octicons";
import Iconn from "react-native-vector-icons/MaterialCommunityIcons";
//import ModalAdd from "./components/ModalAdd";
//import ModalAdd from "./components/ModalAdd";
const DataToShow = [
      {
        id : '1',
        name : 'Building JavaScript-Inspired by Vega Scroll '
      },
      {
        id : '2',
        name : 'Building JavaScript-Inspired by Vega Scroll'
      },
      {
        id : '3',
        name : 'Building JavaScript-Inspired by Vega Scroll'
      },
      {
        id : '4',
        name : 'Building JavaScript-Inspired by Vega Scroll'
      },
      {
        id : '5',
        name : 'Building JavaScript-Inspired by Vega Scroll'
      },
      {
        id : '6',
        name : 'Building JavaScript-Inspired by Vega Scroll'
      },
      {
        id : '7',
        name : 'Building JavaScript-Inspired by Vega Scroll'
      },
      {
        id : '8',
        name : 'Building JavaScript-Inspired by Vega Scroll'
      },
      {
        id : '9',
        name : 'Building JavaScript-Inspired by Vega Scroll'
      }
      
]


state = {
  alarms: []
}



const ExpensesScreenIndex = () => {
    
  //constructor


  

    const [modalOpen, setModalOpen] = useState(false);
    return (
    <View  style={{flex : 1}}>
     
      {/* Recherche */}
   
         {/* <View style={{ 
           height:50, 
          borderBottomColor:'#DCDCDC', 
           borderBottomWidth:1 }}>
          
          
           <Iconn name="calendar-month-outline" color={'#800080'} size={32} style={{ marginTop: 5, marginRight:200}}/>
          
           <Icon name="ios-options" color={'#800080'} size={32} style={{ marginTop: -33, marginLeft:360}}/>
       
         
           <Iconn name="chevron-down" color={'#800080'} size={32} style={{ marginTop: -33}}/>
             
      </View> */}
   
   

   
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center"}}>
        
         <Modal visible={modalOpen} animationType="slide">
           <View >
             
             <TouchableOpacity
               style={styles.button}
               onPress={() => setModalOpen(false)}
              >
                 <Iconn name="close-outline" color={'#800080'} size={25} /> 
             </TouchableOpacity>

           </View>
           <View style={{  justifyContent:'center', alignItems:'center',}}>
             <Text >inside modal</Text>
             <TextInput style={{ margin:10, width: '90%', height:50,}}></TextInput>
             <TextInput style={{ margin:10, width: '90%', height:50,}}></TextInput>
             <TextInput style={{ margin:10, width: '90%', height:50,}}></TextInput>
             <TextInput style={{ margin:10, width: '90%', height:50,}}></TextInput>
             <Button  style={{backgroundColor:'#90486e',}}> OK </Button>
           </View>
           
         </Modal>

         <View>
                <TouchableOpacity
                  
                  style={styles.button}
                  onPress={() => setModalOpen(true)}
                >
                  
                
                <Icons name="diff-added" color={'#008000'} size={40}  />
                  
                </TouchableOpacity>
              

      </View>
        
      </View>

      
     


       <View style={styles.cont}>
               <VegaScrollList data={DataToShow}
               renderItem={({item}) => <View style={{
                 margin:1, 
                 borderWidth: 0,
                 alignItems: 'center',
                 backgroundColor: '#ffffff',
                 borderRadius: 9,
                 shadowRadius: 9,
                 shadowOpacity: 0.5
               
               }}>
               <Iconn name="car-connected" /> 
               <Text style={{
                  margin:10,
                  fontSize: 12,
                  marginLeft: 16,
                  marginTop: 20,
                  fontWeight: '200',
               }}>
              
               { item.name }</Text>
               
               </View>}

               
               keyExtractor={item => item.id}
               style={styles.flatList}
               
   
               />
     
            </View>

   
      </View>
   
    );
};

export default ExpensesScreenIndex;

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    padding: 2,
  

  },
  ModelContent: {
    flex: 1,
    
  },
  cont:{
    marginTop:40,
  }
 
}

);

