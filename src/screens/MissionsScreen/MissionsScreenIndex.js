import React from "react";
import MapView  from 'react-native-maps';

import { StyleSheet,Button, Dimensions, Image, Text,View, TouchableWithoutFeedback } from 'react-native'


const MissionsScreenIndex = () => {
  return (
    <View>
    <View style={styles.container}>
    <MapView style={styles.mapStyle}
    loadingEnabled={true}
    initialRegion={{
      latitude:36.9277958,
      longitude:10.1815426 ,
      latitudeDelta:70,
      longitudeDelta:70,
    }}
   
      >

    </MapView> 
</View>

<View>
 <Text></Text> 
</View>
</View>
  );
};

export default MissionsScreenIndex;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

});