import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Icon from "react-native-vector-icons/Ionicons";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";

import RealTimeIndex from "./RealTimeIndex";
import RunningScreenIndex from "../RunningScreen/RunningScreenIndex";
import StoppedScreenIndex from "../StoppedScreen/StoppedScreenIndex";
import UntrackedScreenIndex from "../UntrackedScreen/UntrackedScreenIndex";

const RealtimeStack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const Navigation = () => (
  <Tab.Navigator initialRouteName="Home" activeColor="#fff">
    <Tab.Screen
      name="Real Time"
      component={RealtimeStackScreen}
      options={{
        tabBarLabel: "",
        tabBarColor: "#71415E",
        tabBarIcon: ({ color }) => (
          // <Icon name="ios-home" color={color} size={26} />
           <Icon name="md-map" color={ color } size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Running"
      component={RunningScreenIndex}
      options={{
        tabBarLabel: "Running",
        tabBarColor: "#B26F97",
        tabBarIcon: ({ color }) => (
          <Icon name="ios-play" color={color} size={26} />
        ),
      }}
    />

    <Tab.Screen
      name="stopped"
      component={StoppedScreenIndex}
      options={{
        tabBarLabel: "Stopped",
        tabBarColor: "#C177A3",
        tabBarIcon: ({ color }) => (
          <Icon name="ios-pause" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Untracked"
      component={UntrackedScreenIndex}
      options={{
        tabBarLabel: "Untracked",
        tabBarColor: "#975B7F",
        tabBarIcon: ({ color }) => (
          <Icon name="ios-alert" color={color} size={26} />

        ),
      }}
    />
  </Tab.Navigator>
);

export default Navigation;

const RealtimeStackScreen = ({ navigation }) => (
  <RealtimeStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#71415E",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <RealtimeStack.Screen
      name="Real Time Tracking"
      component={RealTimeIndex}
      options={{
        title: "Real Time Tracking",
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#71415E"
            onPress={() => navigation.openDrawer()}
          >
            {" "}
          </Icon.Button>
        ),
        headerRight: () => (
          <Icon.Button
            name="ios-list"
            size={25}
            backgroundColor="#71415E"
            onPress={() => {}}
          >
            {" "}
          </Icon.Button>
        ),
      }}
    />
  </RealtimeStack.Navigator>
);
