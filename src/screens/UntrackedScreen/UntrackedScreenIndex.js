import React from "react";
import { View, Text } from "react-native";

const UntrackedScreenIndex = () => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Untracked Trucks</Text>
    </View>
  );
};

export default UntrackedScreenIndex;
