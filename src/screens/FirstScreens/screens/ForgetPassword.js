
import React, { Fragment } from 'react'
import { StyleSheet, SafeAreaView, View, ScrollView, ToastAndroid } from 'react-native'
import { Button } from 'react-native-elements'
import { Formik } from 'formik'
import * as Yup from 'yup'

import FormInput from '../components/FormsComp/FormInput'
import FormButton from '../components/FormsComp/FormButton'
import ErrorMessage from '../components/FormsComp/FormButton'

import NewPassword from './NewPassword'
import { render } from 'react-dom'


const validationSchema = Yup.object().shape({
  email: Yup.string()
    .label('Email')
    .email('Enter a valid email')
    .required('Please enter a registered email'),
  
})


 

export default class ForgetPassword extends React.Component {
  goToLogin = () => this.props.navigation.navigate('Login')
  goToNewPass = () => this.props.navigation.navigate('NewPassword')

  tost= ()=> {

    return ( ToastAndroid.showWithGravityAndOffset('sent, go check your mail...',ToastAndroid.LONG,ToastAndroid.CENTER, 40, 80));
       } 

  handleSubmit = values => {
    if (values.email.length > 0 ) {

      setTimeout(() => {
        this.tost()
      }, 1000)
    }

      //  this.props.navigation.navigate('App')
      
    }
  

  

  render() {
    return (
      <ScrollView>
            <SafeAreaView style={styles.container}>
                <View  style={{ alignItems: "center", marginLeft:20 ,marginTop:90, marginBottom:20, marginRight:50}}>
                </View>
                <Formik
                    initialValues={{ email: '' }}
                     onSubmit={values => {
                        this.handleSubmit(values)
                        }}
                        
                     validationSchema={validationSchema}
                    >
        
                
                        {({
                            handleChange,
                            values,
                            handleSubmit,
                            errors,
                            isValid,
                            touched,
                            handleBlur,
                            isSubmitting
                        }) => (
                        <Fragment>
                            <FormInput
                                name='email'
                                value={values.email}
                                onChangeText={handleChange('email')}
                                placeholder='Enter email'
                                autoCapitalize='none'
                                iconName='ios-mail'
                                iconColor='#90486e'
                                onBlur={handleBlur('email')}
                                // autoFocus
                            />
                            {/* <ErrorMessage errorValue={touched.email && errors.email} />
                            */}
                            
                            <View style={{marginBottom:30, marginTop:0, justifyContent: "center", flex:1, alignItems:"center",}}>

                            <View style={styles.buttonContainer}>
                                <FormButton
                                buttonType='clear'
                                onPress={handleSubmit}
                                title='SEND'
                                buttonColor='#fff'
                                disabled={!isValid || isSubmitting}
                                // loading={isSubmitting}
                                />
                            </View>

                            <Button
                                title="Check the Code"
                                borderRadius={2}
                                color={'#90486e'}
                                onPress={this.goToNewPass}
                                titleStyle={{
                                color: '#90486e'
                                }}
                                type='outline'
                            />

                            </View>
                    </Fragment>
                )}
            </Formik>
        </SafeAreaView>
      </ScrollView>
  
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    borderRadius:30,
    margin:20,
    marginTop:150,
    height:'90%',
    marginBottom:50,
    
   
  },
  buttonContainer: {
    margin:10,
    marginTop:20,
    marginBottom:20,
  
    backgroundColor:'#90486e',
    borderRadius:8,
    height:50,
    width:133,
    justifyContent: "center"
   

  }
})
