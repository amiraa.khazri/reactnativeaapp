

import React, { Fragment } from 'react'
import { StyleSheet, SafeAreaView, View, ScrollView } from 'react-native'
import { Button } from 'react-native-elements'
import { Formik } from 'formik'
import * as Yup from 'yup'

import FormInput from '../components/FormsComp/FormInput'
import FormButton from '../components/FormsComp/FormButton'
import ErrorMessage from '../components/FormsComp/FormButton'
 

const validationSchema = Yup.object().shape({
  codeVerification: Yup.string()
    .label('CodeVerification')
    .required()
    .min(4, 'Verification code must have more than 4 characters '),
  oldPassword: Yup.string()
    .label('oldPassword')
    .required()
    .min(8, 'Password must have more than 4 characters '),
  newPassword: Yup.string()
    .label('newPassword')
    .required()
    .min(8, 'Password must have more than 4 characters '),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password')], 'Confirm Password must matched Password')
    .required('Confirm Password is required')
  
})

export default class NewPassword extends React.Component {
  goToLogin = () => this.props.navigation.navigate('Login')
  
  // if password not null redirect to Home screens just for test Button 
  handleSubmit = values => {
    if (values.newPassword.length > 0 && values.confirmPassword.length > 0) {
      setTimeout(() => {
        this.props.navigation.navigate('App')
      }, 3000)
    }
  }
render(){
    return (
      <ScrollView>
            <SafeAreaView style={styles.container}>
            <View  style={{ marginLeft:20 ,marginTop:10, marginBottom:20, marginRight:50}}>
            </View>

        <Formik
                    initialValues={{
            codeVerification: '',
            oldPassword: '',
            newPassword: '',
            confirmPassword: ''
          }}
          onSubmit={values => {
            this.handleSubmit(values)
          }}
          validationSchema={validationSchema}>
          {({
            handleChange,
            values,
            handleSubmit,
            errors,
            isValid,
            touched,
            handleBlur,
            isSubmitting
          }) => (
            <Fragment>

            <FormInput
                
                name='code'
                value={values.name}
                backgroundColor={'#f0ebee'}
                borderRadius={9}
              
               
                type='clear'
                onChangeText={handleChange('codeVerification')}
                placeholder='Enter The Verification Code'
                // iconName=''                
                iconColor='#FFF'
                onBlur={handleBlur('codeVerification')}
               
                // autoFocus
              />
                         

                    <View style={styles.buttonContainerVerif}>
                         <FormButton
                        
                            buttonType='clear'
                            onPress={handleSubmit}
                            title='VERIFY'
                            buttonColor='#90486e'
                            disabled={isValid }
                            loading={isSubmitting}
                           
                                />
                    </View>
                
                    
             
             <View style={{backgroundColor:'gray', height:3, width: 367,marginTop:-15, marginLeft:1,  marginBottom:28, borderWidth:0.2, }}></View>

            <FormInput
                name='opassword'
                value={values.password}
                disabled={!isSubmitting}
                
                onChangeText={handleChange('oldPassword')}
                placeholder='Enter Your Old Password'
                secureTextEntry
                iconName='ios-lock'
                iconColor='#90486e'
                onBlur={handleBlur('oldPassword')}
              />
             


            <FormInput
                name='password'
                value={values.password}
                disabled={!isSubmitting}
                onChangeText={handleChange('newPassword')}
                placeholder='Enter New Password'
                secureTextEntry
                iconName='ios-lock'
                iconColor='#90486e'
                onBlur={handleBlur('newPassword')}
              />
             

              <FormInput
                name='password'
                value={values.confirmPassword}
                disabled={!isSubmitting}
                onChangeText={handleChange('confirmPassword')}
                placeholder='Confirm Password'
                secureTextEntry
                iconName='ios-lock'
                iconColor='#90486e'
                onBlur={handleBlur('confirmPassword')}
              />
           

             

                            
                            
                            <View style={{marginBottom:30, marginTop:0, justifyContent: "center", flex:1, alignItems:"center",}}>

                            <View style={styles.buttonContainer}>
                                <FormButton
                                buttonType='clear'
                                onPress={handleSubmit}
                                title='CONFIRM'
                                buttonColor='#fff'
                                disabled={!isValid || isSubmitting}
                                // loading={isSubmitting}
                                />
                            </View>

                            <Button
                                title="Login"
                                color={'#90486e'}
                                onPress={this.goToLogin}
                                titleStyle={{
                                color: '#90486e'
                                }}
                                type='clear'
                            />

                            </View>
                    </Fragment>
                )}
            </Formik>
        </SafeAreaView>
      </ScrollView>
    
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    borderRadius:30,
    margin:20,
    marginTop:40,
    
    marginBottom:10,
    
   
  },
  buttonContainer: {
    margin:10,
    marginTop:20,
    marginBottom:20,
  
    backgroundColor:'#90486e',
      borderRadius:20,
    height:50,
    width:160,
    justifyContent: "center"
  }, 
  buttonContainerVerif : {
    marginLeft:140,
    marginTop:-30,
    marginBottom:30,
    backgroundColor:'#f0ebee',
    borderRadius:60,
    height:30,
    width:100,
    justifyContent: "center"



  }
})
