

import React, { Fragment } from 'react'
import { StyleSheet, SafeAreaView, View, ScrollView } from 'react-native'
import { Button } from 'react-native-elements'
import { Formik } from 'formik'
import * as Yup from 'yup'

import FormInput from '../components/FormsComp/FormInput'
import FormButton from '../components/FormsComp/FormButton'
import ErrorMessage from '../components/FormsComp/FormButton'


const validationSchema = Yup.object().shape({
  email: Yup.string()
    .label('Email')
    .email('Enter a valid email')
    .required('Please enter a registered email'),
  password: Yup.string()
    .label('Password')
    .required()
    .min(8, 'Password must have more than 4 characters ')
})

export default class Login extends React.Component {
  goToSignup = () => this.props.navigation.navigate('Signup')
  goToForgetPassword = () => this.props.navigation.navigate('ForgetPassword')

 

  handleSubmit = values => {
    if (values.email.length > 0 && values.password.length > 0) {
      setTimeout(() => {
        this.props.navigation.navigate('App')
      }, 3000)
    }
  }

  render() {
    return (
      <ScrollView >
      <SafeAreaView style={styles.container}>
      <View  style={{ alignItems: "center", marginLeft:20 ,marginTop:90, marginBottom:20, marginRight:50}}>
      </View>
     
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={values => {
            this.handleSubmit(values)
			}}
		
          validationSchema={validationSchema}
		>
          {({
            handleChange,
            values,
            handleSubmit,
            errors,
            isValid,
            touched,
            handleBlur,
            isSubmitting
          }) => (
            <Fragment>
              <FormInput
                name='email'
                value={values.email}
                onChangeText={handleChange('email')}
                placeholder='Enter Email'
                autoCapitalize='none'
                iconName='ios-mail'
                iconColor='#90486e'
                onBlur={handleBlur('email')}
                // autoFocus
              />
              {/* <ErrorMessage errorValue={touched.email && errors.email} />
               */}
              <FormInput
                name='password'
                value={values.password}
                onChangeText={handleChange('password')}
                placeholder='Enter Password'
                secureTextEntry
                iconName='ios-lock'
                iconColor='#90486e'
                onBlur={handleBlur('password')}
              />

              {/* <ErrorMessage errorValue={touched.password && errors.password} /> */}
              <View style={{marginBottom:30, marginTop:0, justifyContent: "center", flex:1, alignItems:"center",}}>

              <View style={styles.buttonContainer}>
                <FormButton
                  buttonType='clear'
                  onPress={handleSubmit}
                  title='LOGIN'
                  buttonColor='#fff'
                  disabled={!isValid || isSubmitting}
                  loading={isSubmitting}
                />
              </View>

              <Button
                title="Don't have an account? Sign Up"
                color={'#90486e'}
                onPress={this.goToSignup}
                titleStyle={{
                  color: '#90486e'
                }}
                type='clear'
              />

              <Button
                title="password forgotten?"
                color={'#90486e'}
                onPress={this.goToForgetPassword}
                titleStyle={{
                  color: '#90486e'
                }}
                type='clear'
              />

              </View>
            </Fragment>
          )}
        </Formik>
        
      </SafeAreaView>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    borderRadius:30,
    margin:20,
    marginTop:50,
    height:'90%',
    marginBottom:50,
   
  },
  buttonContainer: {
    margin:10,
    marginTop:90,
    marginBottom:20,
  
    backgroundColor:'#90486e',
    borderRadius:20,
    height:50,
    width:160,
    justifyContent: "center"
   

  }
})
