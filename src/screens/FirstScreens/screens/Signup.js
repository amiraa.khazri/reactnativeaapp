import React, { Fragment } from 'react'
import { StyleSheet, SafeAreaView, View, ScrollView, Text } from 'react-native'
import { Button } from 'react-native-elements'
import { Formik } from 'formik'
import * as Yup from 'yup'
import FormInput from '../components/FormsComp/FormInput'
import FormButton from '../components/FormsComp/FormButton'
import ErrorMessage from '../components/FormsComp/ErrorMessage'

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .label('Name')
    .required()
    .min(2, 'Must have at least 2 characters'),
  email: Yup.string()
    .label('Email')
    .email('Enter a valid email')
    .required('Please enter a registered email'),
  password: Yup.string()
    .label('Password')
    .required()
    .min(4, 'Password must have more than 4 characters '),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password')], 'Confirm Password must matched Password')
    .required('Confirm Password is required')
})

export default class Signup extends React.Component {
    // export default function Signup(){
  goToLogin = () => this.props.navigation.navigate('Login')

  handleSubmit = values => {
    if (values.email.length > 0 && values.password.length > 0) {
      setTimeout(() => {
        this.props.navigation.navigate('App')
      }, 3000)
    }
  }
render(){
  
    return (
     

      <ScrollView >


      <SafeAreaView style={styles.container} >
      
          <View  style={{ alignItems: "center", marginLeft:20 ,marginTop:25, marginBottom:20, marginRight:50}}>
          </View>
 
        <Formik
          initialValues={{
            name: '',
            email: '',
            password: '',
            confirmPassword: ''
          }}
          onSubmit={values => {
            this.handleSubmit(values)
          }}
          validationSchema={validationSchema}>
          {({
            handleChange,
            values,
            handleSubmit,
            errors,
            isValid,
            touched,
            handleBlur,
            isSubmitting
          }) => (
            <Fragment>
   
           
              <FormInput
                
                name='name'
                value={values.name}
                onChangeText={handleChange('name')}
                placeholder='Enter your Full Name'
                iconName='md-person'
                iconColor='#90486e'
                onBlur={handleBlur('name')}
               
                // autoFocus
              />
              <View style={{ marginTop:-30,}}>
              <ErrorMessage errorValue={touched.name && errors.name} />
              </View>
              <FormInput
                name='email'
                value={values.email}
                onChangeText={handleChange('email')}
                placeholder='Enter Email'
                autoCapitalize='none'
                iconName='ios-mail'
                iconColor='#90486e'
                onBlur={handleBlur('email')}
              />
              <View style={{ marginTop:-30,}}>
               <ErrorMessage errorValue={touched.email && errors.email} />
              </View> 
             <FormInput
                name='password'
                value={values.password}
                onChangeText={handleChange('password')}
                placeholder='Enter Password'
                secureTextEntry
                iconName='ios-lock'
                iconColor='#90486e'
                onBlur={handleBlur('password')}
              />
               <View style={{ marginTop:-30,}}>
              <ErrorMessage errorValue={touched.password && errors.password} />
              </View>
              <FormInput
                name='password'
                value={values.confirmPassword}
                onChangeText={handleChange('confirmPassword')}
                placeholder='Confirm Password'
                secureTextEntry
                iconName='ios-lock'
                iconColor='#90486e'
                onBlur={handleBlur('confirmPassword')}
              />
               <View style={{ marginTop:-30,}}>

              <ErrorMessage
                errorValue={touched.confirmPassword && errors.confirmPassword}
              />
              </View>

           

             <View style={{marginBottom:30, marginTop:0, justifyContent: "center", flex:1, alignItems:"center",}}>
              <View style={styles.buttonContainer}>
                <FormButton
                
                  backgroundColor='#90486e'
                  buttonType='clear'
                  onPress={handleSubmit}
                  title='SIGN UP'
                  buttonColor='#fff'
                  disabled={!isValid || isSubmitting}
                  loading={isSubmitting}
                />
              </View>

              <Button
                title='Have an account? Login'
                onPress={this.goToLogin}
                titleStyle={{
                  color: '#90486e'
                }}
                type='clear'
              />

              </View>
            </Fragment>
            
          )}
        </Formik>
       
      </SafeAreaView>
      </ScrollView>
     
     
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    borderRadius:30,
    margin:20,
    marginTop:50,
    height:'90%',
    
   

  },
  buttonContainer: {
    margin:20,
    backgroundColor:'#90486e',
    borderRadius:20,
    height:50,
    width:160,
    justifyContent: "center"
    
  
  }
})
