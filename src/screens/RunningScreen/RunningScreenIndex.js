import React from "react";
import { View, Text } from "react-native";

const RunningScreenIndex = () => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Running Trucks</Text>
    </View>
  );
};

export default RunningScreenIndex;
