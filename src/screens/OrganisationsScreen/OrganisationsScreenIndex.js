import React from "react";
import { View, Text } from "react-native";

const OrganisationsScreenIndex = () => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Organisations</Text>
    </View>
  );
};

export default OrganisationsScreenIndex;
